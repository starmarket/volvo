export default interface SearchBarProps {
  inputCallback: (input: string) => void
}
