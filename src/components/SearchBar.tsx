import React, { useState } from "react";
import { TextInput, View } from "vcc-ui";
import SearchBarProps from "../interfaces/searchBar";

const SearchBar: React.FC<SearchBarProps> = ({ inputCallback }) => {
  const [value, setValue] = useState('');

  return (
    <View className="searchbar">
      <TextInput
        value={value}
        label="Search"
        onChange={(e) => {
          setValue(e.target.value);
          inputCallback(e.target.value)
        }}
      />
    </View>
  );
};

export default SearchBar;
