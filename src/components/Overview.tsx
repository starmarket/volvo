import React, { useEffect, useState } from "react";
import Link from "next/link";
import Carousel from "../../src/components/Carousel";
import CarouselItem from "../../src/components/CarouselItem";
import { View, Text, Spacer } from "vcc-ui";
import Car from "../../src/interfaces/car";
import OverViewProps from "../interfaces/overview";
import useWindowSize from "../hooks/useWindowSize";
import { breakpoints } from "../constants/breakpoints";

const Overview: React.FC<OverViewProps> = ({ searchTerm }) => {
  const [cars, setCars] = useState([]);
  const { width } = useWindowSize();

  useEffect(() => {
    fetch("/api/cars.json")
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        setCars(res);
      });
  }, []);

  return (
    <Carousel>
      {cars
        .filter((car: Car) => car.bodyType.includes(searchTerm.toLowerCase()))
        .map((car: Car) => {
          return (
            <CarouselItem key={car.id}>
              <View>
                <View>
                  <Text
                    foreground="#707070"
                    subStyle="emphasis"
                    extend={{
                      fontSize: "2rem",
                    }}
                  >
                    {car.bodyType}
                  </Text>
                  <View direction="row">
                    <Text
                      subStyle="emphasis"
                      extend={{
                        fontSize: "2rem",
                      }}
                    >
                      {car.modelName}
                    </Text>
                    <Spacer />
                    <Text
                      foreground="#707070"
                      extend={{
                        fontSize: "2rem",
                      }}
                    >
                      {car.modelType}
                    </Text>
                  </View>
                </View>
                <Spacer size={2} />
                <img
                  src={car.imageUrl}
                  alt={car.modelName}
                  style={{
                    height: width >= breakpoints.bigScreen ? "180px" : "540px",
                  }}
                />
                <Spacer size={2} />
                <View direction="row" justifyContent="center">
                  <View
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Link href={`/learn/${car.id}`} passHref>
                      <Text
                        subStyle="emphasis"
                        foreground={"#6491C6"}
                        className="link"
                        extend={{
                          fontSize: "2rem",
                        }}
                      >
                        LEARN
                      </Text>
                    </Link>
                    <Spacer size={1} />
                    <Link href={`/learn/${car.id}`} passHref>
                      <img
                        src="/images/chevron-small.svg"
                        className={`link ${
                          width >= breakpoints.bigScreen
                            ? "link-arrow"
                            : "link-arrow-mobile"
                        }`}
                        alt="Navigate Left"
                      />
                    </Link>
                  </View>
                  <Spacer size={2} />
                  <View
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Link href={`/shop/${car.id}`} passHref>
                      <Text
                        subStyle="emphasis"
                        foreground={"#6491C6"}
                        className="link"
                        extend={{
                          fontSize: "2rem",
                        }}
                      >
                        SHOP
                      </Text>
                    </Link>
                    <Spacer size={1} />
                    <Link href={`/shop/${car.id}`} passHref>
                      <img
                        src="/images/chevron-small.svg"
                        className={`link ${
                          width >= breakpoints.bigScreen
                            ? "link-arrow"
                            : "link-arrow-mobile"
                        }`}
                        alt="Navigate Right"
                      />
                    </Link>
                  </View>
                </View>
              </View>
            </CarouselItem>
          );
        })}
    </Carousel>
  );
};

export default Overview;
