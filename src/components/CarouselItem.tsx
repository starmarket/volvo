import React from "react";

interface CarouselItemProps {
  width?: string;
}

const CarouselItem: React.FC<CarouselItemProps> = ({ children, width }) => {
  return (
    <div className="carousel-item" style={{ width }}>
      {children}
    </div>
  );
};

export default CarouselItem;
