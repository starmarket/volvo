import React, { useState } from "react";
import { useSwipeable } from "react-swipeable";
import { View } from "vcc-ui";
import { breakpoints } from "../constants/breakpoints";
import useWindowSize from "../hooks/useWindowSize";

const Carousel: React.FC = ({ children }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const { width } = useWindowSize();

  const updateIndex = (newIndex: number): void => {
    if (newIndex < 0) {
      newIndex = 0;
    } else if (
      newIndex >=
      React.Children.count(children) / (width >= breakpoints.bigScreen ? 4 : 1)
    ) {
      newIndex =
        React.Children.count(children) /
          (width >= breakpoints.bigScreen ? 4 : 1) -
        1;
    }
    setActiveIndex(newIndex);
  };

  const handlers = useSwipeable({
    onSwipedLeft: () => updateIndex(activeIndex + 1),
    onSwipedRight: () => updateIndex(activeIndex - 1),
  });

  const calcultatePercentTranslate = (): number => {
    const isLastSlide = activeIndex + 1 === React.Children.count(children);
    return (
      activeIndex *
      (width >= breakpoints.bigScreen ? 100 : isLastSlide ? 78 : 80)
    );
  };

  return (
    <div {...handlers} className="carousel">
      <div
        className="inner"
        style={{ transform: `translateX(-${calcultatePercentTranslate()}%)` }}
      >
        {React.Children.map(children, (child) => {
          if (!React.isValidElement(child)) {
            return child;
          }
          return React.cloneElement(child, {
            width: width >= breakpoints.bigScreen ? "25%" : "80%",
          });
        })}
      </div>
      <View direction="row" className="indicators">
        {width >= breakpoints.bigScreen ? (
          <>
            <img
              src="/images/chevron-circled.svg"
              className="indicator indicator-left"
              onClick={() => {
                updateIndex(activeIndex - 1);
              }}
            />
            <img
              src="/images/chevron-circled.svg"
              className="indicator"
              onClick={() => {
                updateIndex(activeIndex + 1);
              }}
            />
          </>
        ) : (
          <div className="dot-container">
            {React.Children.map(children, (_, index) => {
              return (
                <div
                  className={`dot ${index === activeIndex ? "filled-dot" : ""}`}
                />
              );
            })}
          </div>
        )}
      </View>
    </div>
  );
};

export default Carousel;
