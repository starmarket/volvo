import React from "react";
import "../styles/styles.css";
import AppInterface from "../src/interfaces/app";
import { StyleProvider, ThemePicker, View } from "vcc-ui";

const MyApp: React.FC<AppInterface> = ({ Component, pageProps }) => {
  return (
    <React.StrictMode>
      <StyleProvider>
        <ThemePicker variant="light">
            <Component {...pageProps} />
        </ThemePicker>
      </StyleProvider>
    </React.StrictMode>
  );
};

export default MyApp;
