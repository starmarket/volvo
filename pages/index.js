import React, { useState } from "react";
import { StyleProvider, ThemePicker } from "vcc-ui";
import Overview from "../src/components/Overview";
import SearchBar from "../src/components/SearchBar";
import { breakpoints } from "../src/constants/breakpoints";
import useWindowSize from "../src/hooks/useWindowSize";

function HomePage() {
  const [searchTerm, setSearchTerm] = useState("");
  const { width } = useWindowSize();

  return (
    <React.StrictMode>
      <StyleProvider>
        <ThemePicker variant="light">
          <div
            className={`app-wrapper ${
              width >= breakpoints.bigScreen ? "app-wrapper-big" : ""
            }`}
          >
            <SearchBar inputCallback={setSearchTerm} />
            <Overview searchTerm={searchTerm} />
          </div>
        </ThemePicker>
      </StyleProvider>
    </React.StrictMode>
  );
}

export default HomePage;
