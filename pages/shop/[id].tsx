import React from 'react';
import { useRouter } from 'next/router'
import { Text } from "vcc-ui";

const ShopCar: React.FC = () => {
  const router = useRouter()
  const { id } = router.query

  return <Text>Buy {id}</Text>
}

export default ShopCar