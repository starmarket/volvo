import React from 'react';
import { useRouter } from 'next/router'
import { Text } from "vcc-ui";

const CarDetails: React.FC = () => {
  const router = useRouter()
  const { id } = router.query

  return <Text>Learn more about {id}</Text>
}

export default CarDetails